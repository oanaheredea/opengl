#version 410 core

in vec3 colour;
in vec3 normal;
in vec4 fragPosEye;
in vec2 fragTexCoords;
out vec4 fColor;

//lighting
uniform	mat3 normalMatrix;
uniform	vec3 lightDir;
uniform	vec3 lightColor;
uniform vec3 lightPosEye;
uniform vec3 dominantDirectionalLight;
uniform vec2 lightIntensity;

vec3 ambient;
float ambientStrength = 0.03f;
vec3 diffuse = vec3(0.0f);
vec3 specular = vec3(0.0f);
float specularStrength = 0.005f;
float shininess = 1.0f;

float constant = 1.0f;
float linear = 0.0045f;
float quadratic = 0.0075f;

uniform sampler2D diffuseTexture;
uniform sampler2D specularTexture;

vec3 computeDirectionalLight(vec3 dominantDirectionalLight,vec3 normal,vec3 viewDir)
{

	//transform normal									//new
	vec3 normalEye = normalize(normalMatrix * normal);	//new
	
	vec3 lightDir = normalize(-dominantDirectionalLight);
	//Diffuse shading
	float diff = max(dot(normal, lightDir), 0.0f);
	
	//Specular shading
	vec3 reflectDir = reflect(-lightDir, normal);
	float specCoeff = pow(max(dot(viewDir, reflectDir), 0.0f), shininess);
	//Ambient & Combine
	vec3 ambient = ambientStrength * vec3(texture(diffuseTexture,fragTexCoords));
	vec3 diffuse  = diff * vec3(texture(diffuseTexture,fragTexCoords));
    vec3 specular = specularStrength * specCoeff * vec3(texture(diffuseTexture,fragTexCoords));
	
	return (ambient + diffuse + specular);
}

void computeLightComponents()
{		
	vec3 cameraPosEye = vec3(0.0f);//in eye coordinates, the viewer is situated at the origin
	
	//transform normal
	vec3 normalEye = normalize(normalMatrix * normal);	
	
	//compute light direction
	vec3 lightDirN = normalize(lightPosEye - fragPosEye.xyz);
	
	//compute view direction 
	vec3 viewDirN = normalize(cameraPosEye - fragPosEye.xyz);
		
	//compute distance to light
	float dist = length(lightPosEye - fragPosEye.xyz);
	//compute attenuation
	float att = 1.0f / (constant + linear * dist + quadratic * (dist * dist));
	
	att*=lightIntensity.x;
		
		
	//compute ambient light
	ambient = att * ambientStrength * lightColor;
	ambient *= vec3(texture(diffuseTexture, fragTexCoords));
	
	//compute diffuse light
	diffuse = att * max(dot(normalEye, lightDirN), 0.0f) * lightColor;
	diffuse *= vec3(texture(diffuseTexture, fragTexCoords));

	
	//compute specular light
	
	//Blinn Specular
	vec3 reflection = reflect(-lightDirN, normalEye);
	float specCoeff = pow(max(dot(viewDirN, reflection), 0.0f), shininess);

	//Blinn-Phong Specular	
	//compute half vector
	//vec3 halfVector = normalize(lightDirN + viewDirN);
	//float specCoeff = pow(max(dot(normal,halfVector), 0.0), 16.0); 
	
	specular = att * specularStrength * specCoeff * lightColor;
	specular *= vec3(texture(specularTexture, fragTexCoords));
}

float computeFog() 
{ 
	float fogDensity = 0.00005f; 
	float fragmentDistance = length(fragPosEye); 
	float fogFactor = exp(-pow(fragmentDistance * fogDensity, 2)); 
	return clamp(fogFactor, 0.0f, 1.0f); 
}

void main() 
{
	fColor = texture(diffuseTexture, fragTexCoords);
	computeLightComponents();
	vec3 norm = normalize(normal);
	
	/*
	vec3 baseColor = vec3(1.0f, 1.0f, 1.0f);//orange
	ambient *= baseColor;
	diffuse *= baseColor;
	specular *= baseColor;
	*/
	
	vec3 cameraPosEye = vec3(0.0f);
	
	vec3 sunLight = computeDirectionalLight(dominantDirectionalLight,norm,normalize(cameraPosEye - fragPosEye.xyz));
	vec3 color = min(((ambient + diffuse) + specular) + sunLight, 1.0f);
    
	float fogFactor = computeFog(); 
	vec4 fogColor = vec4(0.1f, 0.1f, 0.1f, 1.0f); 
	fColor = mix(fogColor, vec4(color, 1.0f), fogFactor);
    //fColor = vec4(color, 1.0f); //NO FOG
}
