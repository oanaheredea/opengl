//
//  Camera.cpp
//  Lab5
//
//  Created by CGIS on 28/10/2016.
//  Copyright © 2016 CGIS. All rights reserved.
//

#include "Camera.hpp"

namespace gps {

	glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
    
    Camera::Camera(glm::vec3 cameraPosition, glm::vec3 cameraTarget)
    {
        this->cameraPosition = cameraPosition;
        this->cameraTarget = cameraTarget;
        this->cameraDirection = glm::normalize(cameraTarget - cameraPosition);
        this->cameraRightDirection = glm::normalize(glm::cross(this->cameraDirection, glm::vec3(0.0f, 1.0f, 0.0f)));
    }
    
    glm::mat4 Camera::getViewMatrix()
    {
        return glm::lookAt(cameraPosition, cameraPosition + cameraFront , glm::vec3(0.0f, 1.0f, 0.0f));
    }
    
	glm::vec3 Camera::getCameraTarget()
	{
		return cameraTarget;
	}

    void Camera::move(MOVE_DIRECTION direction, float speed)
    {
        switch (direction) {
            case MOVE_FORWARD:
                cameraPosition += cameraDirection * speed;
                break;
                
            case MOVE_BACKWARD:
                cameraPosition -= cameraDirection * speed;
                break;
                
            case MOVE_RIGHT:
                cameraPosition += cameraRightDirection * speed;
                break;
                
            case MOVE_LEFT:
                cameraPosition -= cameraRightDirection * speed;
                break;

			case MOVE_UP:
				cameraPosition += cameraUp * speed;
				break;

			case MOVE_DOWN:
				cameraPosition -= cameraUp * speed;
				break;
        }
    }
    
    void Camera::rotate(float pitch, float yaw)
    {
		
		glm::mat4 rotationMatrix = glm::mat4(1.0f);
		glm::vec4 cameraDirectionPoint = glm::vec4(cameraDirection, 1.0f);

		rotationMatrix = glm::rotate(rotationMatrix, yaw, glm::vec3(0.0f, 1.0f, 0.0f));
		rotationMatrix = glm::rotate(rotationMatrix, pitch, cameraRightDirection);

		cameraDirectionPoint = rotationMatrix * cameraDirectionPoint;
		glm::normalize(cameraDirectionPoint);

		cameraDirection = (glm::vec3)cameraDirectionPoint;
		cameraRightDirection = glm::cross(cameraDirection, glm::vec3(0.0f, 1.0f, 0.0f));
		
		glm::vec3 front;
		front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
		front.y = sin(glm::radians(pitch));
		front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
		cameraFront = glm::normalize(front);
    }
    
}
