//
//  main.cpp
//  OpenGL Advances Lighting
//
//  Created by CGIS on 28/11/16.
//  Copyright � 2016 CGIS. All rights reserved.
//	Oana Heredea

#define GLEW_STATIC

#include <iostream>
#include "glm/glm.hpp"//core glm functionality
#include "glm/gtc/matrix_transform.hpp"//glm extension for generating common transformation matrices
#include "glm/gtc/matrix_inverse.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "GLEW/glew.h"
#include "GLFW/glfw3.h"
#include <string>
#include "Shader.hpp"
#include "Camera.hpp"
#include "SkyBox.hpp"
#define TINYOBJLOADER_IMPLEMENTATION

#include "Model3D.hpp"
#include "Mesh.hpp"

int glWindowWidth = 1920;
int glWindowHeight = 1080;
int retina_width, retina_height;
GLFWwindow* glWindow = NULL;
bool stopAnimation = true;


glm::mat4 model, model5, model6, model7, model8, model9;
GLuint modelLoc, modelLoc5, modelLoc6, modelLoc7, modelLoc8, modelLoc9;
glm::mat4 view;
GLuint viewLoc;
glm::mat4 projection;
GLuint projectionLoc;
glm::mat3 normalMatrix;
GLuint normalMatrixLoc;

glm::vec3 lightDir;
GLuint lightDirLoc;
glm::vec3 lightColor;
GLuint lightColorLoc;
glm::vec3 dominantDirectionalLight;
GLuint dominantDirectionalLightLoc;
glm::vec3 lightPosEye;
GLuint lightPosEyeLoc;
glm::vec2 lightIntensity;
GLuint lightIntensityLoc;
GLfloat lightAngle;
float enableFog = 1.0f;
GLuint enableFogLoc;

//SHADOWS
const GLuint SHADOW_WIDTH = 1920, SHADOW_HEIGHT = 1080;

gps::Camera myCamera(glm::vec3(0.0f, 0.0f, 2.5f), glm::vec3(0.0f, 0.0f, -10.0f));
float cameraSpeed = 0.1;

bool pressedKeys[1024];
float angle = 0.0f;

gps::Model3D spaceShip;
gps::Model3D jupiter;
gps::Model3D moon;
gps::Model3D asteroids;
gps::Model3D sun;
gps::Model3D earth;
gps::Model3D brian;
gps::Model3D neptun;


gps::Shader myCustomShader;
gps::Shader lightShader;
gps::Shader depthMapShader;

GLuint shadowMapFBO;
GLuint depthMapTexture;

gps::SkyBox mySkyBox;
gps::Shader skyboxShader;
std::vector<const GLchar*> faces;

//MOUSE CALLBACK
GLfloat yaw = -90.0f;	// Yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right (due to how Eular angles work) so we initially rotate a bit to the left.
GLfloat pitch = 0.0f;
GLfloat lastX = glWindowWidth / 2.0;
GLfloat lastY = glWindowHeight / 2.0;
bool firstMouse = true;
float intensity = 10;
float PI = 3.14159265359;
float revolutionMoon;
float resolutionMoon;


GLenum glCheckError_(const char *file, int line)
{
	GLenum errorCode;
	while ((errorCode = glGetError()) != GL_NO_ERROR)
	{
		std::string error;
		switch (errorCode)
		{
		case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
		case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
		case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
		case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
		case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
		case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
		case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
		}
		std::cout << error << " | " << file << " (" << line << ")" << std::endl;
	}
	return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)

void windowResizeCallback(GLFWwindow* window, int width, int height)
{
	fprintf(stdout, "window resized to width: %d , and height: %d\n", width, height);
	//TODO
	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);
	myCustomShader.useShaderProgram();
	//set projection matrix
	glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	//send matrix data to shader
	GLint projLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));

	//set Viewport transform
	glViewport(0, 0, retina_width, retina_height);
}

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
			pressedKeys[key] = true;
		else if (action == GLFW_RELEASE)
			pressedKeys[key] = false;
	}
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos;
	lastX = xpos;
	lastY = ypos;

	float sensitivity = 0.05;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	yaw += xoffset;
	pitch += yoffset;

	if (pitch > 89.0f)
		pitch = 89.0f;
	if (pitch < -89.0f)
		pitch = -89.0f;
	myCamera.rotate(pitch, yaw);
}

float delta2 = 0;
float movementSpeed2 = 0.01; // units per second
void updateDelta2(double elapsedSeconds) {
	delta2 = delta2 + movementSpeed2 * elapsedSeconds;
}
double lastTimeStamp2 = glfwGetTime();

void processMovement()
{
	//ANIMATION
	if (pressedKeys[GLFW_KEY_SPACE]) {
		myCamera = gps::Camera(glm::vec3(0.0f, 4.0f, 0.0f), glm::vec3(0.0f, 4.0f, -10.0f));
		stopAnimation = true;
	}
	
	if (pressedKeys[GLFW_KEY_ENTER]) {
		myCamera = gps::Camera(glm::vec3(0.0f, 50.0f, 150.0f), glm::vec3(0.0f, 10.0f, 10.0f));
		stopAnimation = false;
	}
	
	if (!stopAnimation){
		double currentTimeStamp = glfwGetTime();
		updateDelta2(currentTimeStamp - lastTimeStamp2);
		lastTimeStamp2 = currentTimeStamp;
		myCamera.move(gps::MOVE_FORWARD, delta2);
	}
	//FOG
	if (pressedKeys[GLFW_KEY_F]) {
		enableFog = 0.0f;
		GLint enableFogLoc = glGetUniformLocation(myCustomShader.shaderProgram, "enableFog");
		glUniform1f(enableFogLoc, enableFog);
	}
	if (pressedKeys[GLFW_KEY_G]) {
		enableFog = 1.0f;
		GLint enableFogLoc = glGetUniformLocation(myCustomShader.shaderProgram, "enableFog");
		glUniform1f(enableFogLoc, enableFog);
	}

	if (pressedKeys[GLFW_KEY_Q]) {
		angle += 1.0f;
		if (angle > 360.0f)
			angle -= 360.0f;
	}

	if (pressedKeys[GLFW_KEY_E]) {
		angle -= 1.0f;
		if (angle < 0.0f)
			angle += 360.0f;
	}

	if (pressedKeys[GLFW_KEY_W]) {
		myCamera.move(gps::MOVE_FORWARD, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_S]) {
		myCamera.move(gps::MOVE_BACKWARD, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_A]) {
		myCamera.move(gps::MOVE_LEFT, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_D]) {
		myCamera.move(gps::MOVE_RIGHT, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_I]) {
		intensity += 50.0f;
	}

	if (pressedKeys[GLFW_KEY_O]) {
		intensity -= 50.0f;
	}

	if (pressedKeys[GLFW_KEY_J]) {

		lightAngle += 0.3f;
		if (lightAngle > 360.0f)
			lightAngle -= 360.0f;
		glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
		myCustomShader.useShaderProgram();
		glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDirTr));
	}

	if (pressedKeys[GLFW_KEY_L]) {
		lightAngle -= 0.3f;
		if (lightAngle < 0.0f)
			lightAngle += 360.0f;
		glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
		myCustomShader.useShaderProgram();
		glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDirTr));
	}

	/*
	//ROTATE CAMERA
	//up
	if (pressedKeys[GLFW_KEY_UP]) {
		myCamera.rotate(0.001f, 0.0f);
	}
	//down
	if (pressedKeys[GLFW_KEY_DOWN]) {
		myCamera.rotate(-0.001f, 0.0f);
	}
	//left
	if (pressedKeys[GLFW_KEY_LEFT]) {
		myCamera.rotate(0.0f, 0.001f);
	}
	//right
	if (pressedKeys[GLFW_KEY_LEFT]) {
		myCamera.rotate(0.0f, -0.001f);
	}
	*/
	
}

bool initOpenGLWindow()
{
	if (!glfwInit()) {
		fprintf(stderr, "ERROR: could not start GLFW3\n");
		return false;
	}

	glfwWindowHint(GLFW_SAMPLES, 16);
	glEnable(GL_MULTISAMPLE);

	//for Mac OS X
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glWindow = glfwCreateWindow(glWindowWidth, glWindowHeight, "OpenGL Shader Example", NULL, NULL);
	if (!glWindow) {
		fprintf(stderr, "ERROR: could not open window with GLFW3\n");
		glfwTerminate();
		return false;
	}

	glfwSetWindowSizeCallback(glWindow, windowResizeCallback);
	glfwMakeContextCurrent(glWindow);

	glfwWindowHint(GLFW_SAMPLES, 4);

	// start GLEW extension handler
	glewExperimental = GL_TRUE;
	glewInit();

	// get version info
	const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
	const GLubyte* version = glGetString(GL_VERSION); // version as a string
	printf("Renderer: %s\n", renderer);
	printf("OpenGL version supported %s\n", version);

	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);

	glfwSetKeyCallback(glWindow, keyboardCallback);
	glfwSetCursorPosCallback(glWindow, mouseCallback);
    glfwSetInputMode(glWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	return true;
}

void initOpenGLState()
{
	glClearColor(0.3, 0.3, 0.3, 1.0);
	glViewport(0, 0, retina_width, retina_height);

	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	glEnable(GL_CULL_FACE); // cull face
	glCullFace(GL_BACK); // cull back face
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise
}

void initFBOs()
{
	//generate FBO ID
	glGenFramebuffers(1, &shadowMapFBO);

	//create depth texture for FBO
	glGenTextures(1, &depthMapTexture);
	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
		SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//attach texture to FBO
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMapTexture, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

glm::mat4 computeLightSpaceTrMatrix()
{
	const GLfloat near_plane = 1.0f, far_plane = 10.0f;
	glm::mat4 lightProjection = glm::ortho(-20.0f, 20.0f, -20.0f, 20.0f, near_plane, far_plane);

	glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
	glm::mat4 lightView = glm::lookAt(lightDirTr, myCamera.getCameraTarget(), glm::vec3(0.0f, 1.0f, 0.0f));

	return lightProjection * lightView;
}

void initModels()
{
	moon = gps::Model3D("objects/Moon.obj", "objects/");
	earth = gps::Model3D("objects/earth/earth.obj", "objects/earth/");
	sun = gps::Model3D("objects/sun/sun.obj", "objects/sun/");
	spaceShip = gps::Model3D("objects/akira/akira.obj", "objects/akira/");
	asteroids = gps::Model3D("objects/rocks/rocks.obj", "objects/rocks/");
	jupiter = gps::Model3D("objects/jupiter/jupiter.obj", "objects/jupiter/");
	brian = gps::Model3D("objects/bm/brianmay.obj", "objects/bm/");
	neptun = gps::Model3D("objects/neptune/neptune.obj", "objects/neptune/");


}

GLuint ReadTextureFromFile(const char* file_name) {

	int x, y, n;
	int force_channels = 4;
	unsigned char* image_data = stbi_load(file_name, &x, &y, &n, force_channels);
	if (!image_data) {
		fprintf(stderr, "ERROR: could not load %s\n", file_name);
		return false;
	}
	// NPOT check
	if ((x & (x - 1)) != 0 || (y & (y - 1)) != 0) {
		fprintf(
			stderr, "WARNING: texture %s is not power-of-2 dimensions\n", file_name
		);
	}

	int width_in_bytes = x * 4;
	unsigned char *top = NULL;
	unsigned char *bottom = NULL;
	unsigned char temp = 0;
	int half_height = y / 2;

	for (int row = 0; row < half_height; row++) {
		top = image_data + row * width_in_bytes;
		bottom = image_data + (y - row - 1) * width_in_bytes;
		for (int col = 0; col < width_in_bytes; col++) {
			temp = *top;
			*top = *bottom;
			*bottom = temp;
			top++;
			bottom++;
		}
	}

	GLuint textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(
		GL_TEXTURE_2D,
		0,
		GL_SRGB, //GL_SRGB,//GL_RGBA,
		x,
		y,
		0,
		GL_RGBA,
		GL_UNSIGNED_BYTE,
		image_data
	);
	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	return textureID;
}

void createModel() {

	model = glm::mat4(1.0f);
	modelLoc = glGetUniformLocation(myCustomShader.shaderProgram, "model");
	processMovement();
	myCustomShader.useShaderProgram();
	lightPosEye = view * glm::vec4(lightDir, 1.0f);

	glm::mat4 view = myCamera.getViewMatrix();
	GLint viewLoc = glGetUniformLocation(myCustomShader.shaderProgram, "view");
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	glActiveTexture(GL_TEXTURE0);
	glUniform1i(glGetUniformLocation(myCustomShader.shaderProgram, "diffuseTexture"), 0);
}

void initShaders()
{
	myCustomShader.loadShader("shaders/shaderStart.vert", "shaders/shaderStart.frag");
	myCustomShader.useShaderProgram();
	depthMapShader.loadShader("shaders/simpleDepthMap.vert", "shaders/simpleDepthMap.frag");
	mySkyBox.Load(faces);
	skyboxShader.loadShader("shaders/skyboxShader.vert", "shaders/skyboxShader.frag");
	skyboxShader.useShaderProgram();
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "view"), 1, GL_FALSE,
		glm::value_ptr(view));

	projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "projection"), 1, GL_FALSE,
		glm::value_ptr(projection));
}

void initUniforms()
{
	myCustomShader.useShaderProgram();
	model = glm::mat4(1.0f);
	modelLoc = glGetUniformLocation(myCustomShader.shaderProgram, "model");
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	view = myCamera.getViewMatrix();
	viewLoc = glGetUniformLocation(myCustomShader.shaderProgram, "view");
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	normalMatrixLoc = glGetUniformLocation(myCustomShader.shaderProgram, "normalMatrix");
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	
	projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	projectionLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

	//set the light direction (direction towards the light)
	lightDir = glm::vec3(0.0f, 1.0f, 1.0f);
	lightDirLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightDir");
	glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDir));

	//set light color
	lightColor = glm::vec3(1.0f, 1.0f, 1.0f); //white light
	lightColorLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightColor");
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));

	//set Dominant Directional Light position
	dominantDirectionalLight = glm::vec3(1, -1, 0);
	dominantDirectionalLightLoc = glGetUniformLocation(myCustomShader.shaderProgram, "dominantDirectionalLight");
	glUniform3fv(dominantDirectionalLightLoc, 1, glm::value_ptr(dominantDirectionalLight));
	
	//set Point Light intensity
	lightIntensity = glm::vec2(intensity, 0);
	lightIntensityLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightIntensity");
	glUniform2fv(lightIntensityLoc, 1, glm::value_ptr(lightIntensity));

	GLint enableFogLoc = glGetUniformLocation(myCustomShader.shaderProgram, "enableFog");
	glUniform1f(enableFogLoc, enableFog);
	
}
float xPos = 5;
float yPos = 5;
float zPos = 5;

float delta = 0;

float movementSpeed = 0.1; // units per second

void updateDelta(double elapsedSeconds)
{
	delta = delta + movementSpeed * elapsedSeconds;
}

double lastTimeStamp = glfwGetTime();

float dogX = 0;
float dogY = 0;
float dogZ = 0;

void renderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	processMovement();
	myCustomShader.useShaderProgram();
	//initialize the view matrix
	view = myCamera.getViewMatrix();
	//send view matrix data to shader  
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

	//initialize the model matrix
	model = glm::mat4(1.0f);
	//create model matrix
	model = glm::rotate(model, glm::radians(angle), glm::vec3(0, 1, 0));
	//send model matrix data to shader 
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	//create normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	createModel();
	sun.Draw(myCustomShader);
	
	//MOON
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(50, 10, -350));
	model = glm::rotate(model, -revolutionMoon, glm::vec3(0, 1, 0));
	revolutionMoon += 0.001f;
	model = glm::translate(model, glm::vec3(-150, 0, 0));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	model = glm::rotate(model, resolutionMoon, glm::vec3(0, 1, 0));
	resolutionMoon += 0.01f;

	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	moon.Draw(myCustomShader);
	skyboxShader.useShaderProgram();

	model = glm::mat4(1.0f);
	modelLoc = glGetUniformLocation(myCustomShader.shaderProgram, "model");
	processMovement();
	myCustomShader.useShaderProgram();
	glm::mat4 view4 = myCamera.getViewMatrix();
	GLint viewLoc4 = glGetUniformLocation(myCustomShader.shaderProgram, "view");
	glUniformMatrix4fv(viewLoc4, 1, GL_FALSE, glm::value_ptr(view4));

	double currentTimeStamp = glfwGetTime();
	updateDelta(currentTimeStamp - lastTimeStamp);
	lastTimeStamp = currentTimeStamp;

	model = glm::rotate(model, delta, glm::vec3(0, 1, 0));
	model = glm::translate(model, glm::vec3(xPos, yPos, zPos));
	model = glm::scale(model, glm::vec3(0.7f, 0.7f, 0.7f));

	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	glActiveTexture(GL_TEXTURE0);
	glUniform1i(glGetUniformLocation(myCustomShader.shaderProgram, "diffuseTexture"), 0);
	earth.Draw(myCustomShader);
	skyboxShader.useShaderProgram();

	//set Point Light position
	lightPosEye = glm::vec3(-200, -250, -50);
	lightPosEyeLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightPosEye");
	glUniform3fv(lightPosEyeLoc, 1, glm::value_ptr(lightPosEye));

	//set Point Light intensity
	lightIntensity = glm::vec2(intensity, 0);
	lightIntensityLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightIntensity");
	glUniform2fv(lightIntensityLoc, 1, glm::value_ptr(lightIntensity));

	model5 = glm::mat4(1.0f);
	modelLoc5 = glGetUniformLocation(myCustomShader.shaderProgram, "model");
	processMovement();
	myCustomShader.useShaderProgram();

	glm::mat4 view5 = myCamera.getViewMatrix();
	GLint viewLoc5 = glGetUniformLocation(myCustomShader.shaderProgram, "view");
	glUniformMatrix4fv(viewLoc5, 1, GL_FALSE, glm::value_ptr(view5));

	model5 = glm::translate(model5, glm::vec3(dogX + angle / 10, dogY, dogZ + angle/10));
	glUniformMatrix4fv(modelLoc5, 1, GL_FALSE, glm::value_ptr(model5));

	glActiveTexture(GL_TEXTURE0);
	glUniform1i(glGetUniformLocation(myCustomShader.shaderProgram, "diffuseTexture"), 0);
	spaceShip.Draw(myCustomShader);

	model6 = glm::mat4(1.0f);
	modelLoc6 = glGetUniformLocation(myCustomShader.shaderProgram, "model");
	processMovement();
	myCustomShader.useShaderProgram();

	glm::mat4 view6 = myCamera.getViewMatrix();
	GLint viewLoc6 = glGetUniformLocation(myCustomShader.shaderProgram, "view");
	glUniformMatrix4fv(viewLoc6, 1, GL_FALSE, glm::value_ptr(view6));

	model6 = glm::translate(model6, glm::vec3(dogX + angle / 100, dogY, dogZ + angle / 100));
	model6 = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	glUniformMatrix4fv(modelLoc6, 1, GL_FALSE, glm::value_ptr(model6));

	glActiveTexture(GL_TEXTURE0);
	glUniform1i(glGetUniformLocation(myCustomShader.shaderProgram, "diffuseTexture"), 0);
	asteroids.Draw(myCustomShader);

	model7 = glm::mat4(1.0f);
	modelLoc7 = glGetUniformLocation(myCustomShader.shaderProgram, "model");
	processMovement();
	myCustomShader.useShaderProgram();

	glm::mat4 view7 = myCamera.getViewMatrix();
	GLint viewLoc7 = glGetUniformLocation(myCustomShader.shaderProgram, "view");
	glUniformMatrix4fv(viewLoc7, 1, GL_FALSE, glm::value_ptr(view7));

	model7 = glm::rotate(model7, delta, glm::vec3(0, 1, 0));
	model7 = glm::translate(model7, glm::vec3(-5, 10, 35));
	model7 = glm::scale(model7, glm::vec3(0.7f, 0.7f, 0.7f));
	glUniformMatrix4fv(modelLoc7, 1, GL_FALSE, glm::value_ptr(model7));

	glActiveTexture(GL_TEXTURE0);
	glUniform1i(glGetUniformLocation(myCustomShader.shaderProgram, "diffuseTexture"), 0);
	jupiter.Draw(myCustomShader);

	model8 = glm::mat4(1.0f);
	modelLoc8 = glGetUniformLocation(myCustomShader.shaderProgram, "model");
	processMovement();
	myCustomShader.useShaderProgram();

	glm::mat4 view8 = myCamera.getViewMatrix();
	GLint viewLoc8 = glGetUniformLocation(myCustomShader.shaderProgram, "view");
	glUniformMatrix4fv(viewLoc8, 1, GL_FALSE, glm::value_ptr(view8));

	model8 = glm::rotate(model8, delta, glm::vec3(0, 1, 0));
	model8 = glm::translate(model8, glm::vec3(-50, 29, 35));
	model8 = glm::scale(model8, glm::vec3(1.2f, 1.2f, 1.2f));
	glUniformMatrix4fv(modelLoc8, 1, GL_FALSE, glm::value_ptr(model8));

	glActiveTexture(GL_TEXTURE0);
	glUniform1i(glGetUniformLocation(myCustomShader.shaderProgram, "diffuseTexture"), 0);
	brian.Draw(myCustomShader);

	model9 = glm::mat4(1.0f);
	modelLoc9 = glGetUniformLocation(myCustomShader.shaderProgram, "model");
	processMovement();
	myCustomShader.useShaderProgram();

	glm::mat4 view9 = myCamera.getViewMatrix();
	GLint viewLoc9 = glGetUniformLocation(myCustomShader.shaderProgram, "view");
	glUniformMatrix4fv(viewLoc9, 1, GL_FALSE, glm::value_ptr(view9));

	model9 = glm::rotate(model9, delta, glm::vec3(0, 1, 0));
	model9 = glm::translate(model9, glm::vec3(50, 50, 50));
	model9 = glm::scale(model9, glm::vec3(3.5f, 3.5f, 3.5f));
	glUniformMatrix4fv(modelLoc9, 1, GL_FALSE, glm::value_ptr(model9));

	glActiveTexture(GL_TEXTURE0);
	glUniform1i(glGetUniformLocation(myCustomShader.shaderProgram, "diffuseTexture"), 0);
	neptun.Draw(myCustomShader);

	mySkyBox.Draw(skyboxShader, view, projection);
	myCustomShader.useShaderProgram();

}

/*
void renderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.4, 0.9, 1, 1.0);
	processMovement();
	depthMapShader.useShaderProgram();

	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "lightSpaceTrMatrix"),
		1,
		GL_FALSE,
		glm::value_ptr(computeLightSpaceTrMatrix()));

	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);

	//create model matrix for nanosuit
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
	//send model matrix to shader
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"),
		1,
		GL_FALSE,
		glm::value_ptr(model));

	moon.Draw(depthMapShader);

	myCustomShader.useShaderProgram();
	//initialize the view matrix
	view = myCamera.getViewMatrix();
	//send view matrix data to shader	
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

	//initialize the model matrix
	model = glm::mat4(1.0f);
	//create model matrix
	model = glm::rotate(model, glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	//create normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));

	//createModel();
	//model = glm::translate(model, glm::vec3(200, 0, -100));
	//MOON
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(50, 10, -350));
	model = glm::rotate(model, -revolutionMoon, glm::vec3(0, 1, 0));
	revolutionMoon += 0.001f;
	model = glm::translate(model, glm::vec3(-150, 0, 0));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	model = glm::rotate(model, resolutionMoon, glm::vec3(0, 1, 0));
	resolutionMoon += 0.01f;

	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	moon.Draw(myCustomShader);
	skyboxShader.useShaderProgram();

	createModel();
	jupiter.Draw(myCustomShader);

	skyboxShader.useShaderProgram();

	//set Point Light position
	lightPosEye = glm::vec3(-200, -250, -50);
	lightPosEyeLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightPosEye");
	glUniform3fv(lightPosEyeLoc, 1, glm::value_ptr(lightPosEye));

	//set Point Light intensity
	lightIntensity = glm::vec2(intensity, 0);
	lightIntensityLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightIntensity");
	glUniform2fv(lightIntensityLoc, 1, glm::value_ptr(lightIntensity));


	mySkyBox.Draw(skyboxShader, view, projection);
	myCustomShader.useShaderProgram();

}
*/
int main(int argc, const char * argv[]) {

	faces.push_back("skybox/bkg1_right1.png");
	faces.push_back("skybox/bkg1_left2.png");
	faces.push_back("skybox/bkg1_top3.png");
	faces.push_back("skybox/bkg1_bottom4.png");
	faces.push_back("skybox/bkg1_front5.png");
	faces.push_back("skybox/bkg1_back6.png");
	initOpenGLWindow();
	initOpenGLState();
	initModels();
	initShaders();
	initUniforms();	
	
	
	while (!glfwWindowShouldClose(glWindow)) {
		renderScene();

		glfwPollEvents();
		glfwSwapBuffers(glWindow);
	}

	//close GL context and any other GLFW resources
	glfwTerminate();

	return 0;
}
